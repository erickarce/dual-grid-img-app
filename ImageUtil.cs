﻿using System;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using Windows.Storage.Streams;
using Windows.Storage;
using Windows.UI.Xaml.Media.Imaging;
using Windows.Storage.Pickers;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Windows.Graphics.Imaging;
using Windows.Storage.FileProperties;
using Windows.UI.Xaml.Media;

namespace ImgGripApp
{
    class ImageUtil
    {

        // Image load is currently slow for large images, optimization 
        // is needed here for thumbnails

        public static Image GetImg(StorageFile file)
        {
            BitmapImage bitmapImage = new BitmapImage();
            IRandomAccessStream stream = null;
            Task t = Task.Run(async () =>
            {
                stream = await file.OpenReadAsync();
            });
            t.Wait();
            bitmapImage.SetSource(stream);
            bitmapImage.DecodePixelWidth = 250;

            Image myImage = new Image();
            myImage.Source = bitmapImage;
            myImage.Width = 250;
            return myImage;
        }

        public async static Task<StorageFolder> SelectFolderWithPicker()
        {
            FolderPicker openPicker = new FolderPicker();
            openPicker.ViewMode = PickerViewMode.Thumbnail;
            openPicker.SuggestedStartLocation = PickerLocationId.Desktop;
            openPicker.FileTypeFilter.Add("*");

            StorageFolder folder = await openPicker.PickSingleFolderAsync();
            if (folder == null)
            {
                return null;
            }
            Windows.Storage.AccessCache.StorageApplicationPermissions.FutureAccessList.AddOrReplace("SelectedFolder1Token", folder);
            return folder;
        }

        // Pictures added to end will trigger removal of pictures at the front 
        // of the observable collection to keep a smaller paginated memory 
        // footprint. To keep track of this start and end, indices are needed for
        // for subsequent image loads.

        public static void AddPicToEnd(IReadOnlyList<StorageFile> pictures, int maxPageSize, int index, 
            ObservableCollection<Image> images, ref int pageStart, ref int pageEnd)
        {
            if (index >= pictures.Count)
            {
                return;
            }
            StorageFile file = pictures.ElementAt(index);
            images.Add(ImageUtil.GetImg(file));
            pageEnd++;

            if (images.Count > maxPageSize)
            {
                images.RemoveAt(0);
                pageStart++;
            }
        }

        // Pictures added to front will trigger removal of pictures at the end 
        // of the observable collection to keep a smaller paginated memory 
        // footprint. To keep track of this start and end, indices are needed for
        // for subsequent image loads.

        public static void AddPicToFront(IReadOnlyList<StorageFile> pictures, int maxPageSize, int index,
            ObservableCollection<Image> images, ref int pageStart, ref int pageEnd)
        {
            if (index <= 0)
            {
                return;
            }
            StorageFile file = pictures.ElementAt(index);
            images.Insert(0, ImageUtil.GetImg(file));
            pageStart--;

            if (images.Count > maxPageSize)
            {
                images.RemoveAt(images.Count - 1);
                pageEnd--;
            }
        }

        public static void AddPicsToEnd(IReadOnlyList<StorageFile> pictures, int maxPageSize, int amtToLoad,
            ObservableCollection<Image> images, ref int pageStart, ref int pageEnd)
        {
            for (int i = 0; i < amtToLoad; i++)
            {
                AddPicToEnd(pictures, maxPageSize, pageEnd, images, ref pageStart, ref pageEnd);
            }
        }

        public static void AddPicsToFront(IReadOnlyList<StorageFile> pictures, int maxPageSize, int amtToLoad, 
            ObservableCollection<Image> images, ref int pageStart, ref int pageEnd)
        {
            for (int i = 0; i < amtToLoad; i++)
            {
                AddPicToFront(pictures, maxPageSize, pageStart, images, ref pageStart, ref pageEnd);
            }
        }
    }
}
