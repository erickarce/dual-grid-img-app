﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage;
using Windows.Storage;
using System.IO;

namespace ImgGripApp
{
    class AzureStore
    {

        private readonly CloudBlobClient client;

        private readonly String container;

        public AzureStore(String storageConnectionString, String container)
        {
            CloudStorageAccount account = CloudStorageAccount.Parse(storageConnectionString);
            client = account.CreateCloudBlobClient();
            this.container = container;
        }

        // Creates resource in Azure if does not exist and waits for creation 
        // to finish before uploading files one by one. If the file exists on
        // Azure it will get removed first.

        // Files are asynchronously uploaded and a task list makes sure all files
        // are finished before returning.

        public async Task UploadFilesAsync(StorageFolder dir)
        {
            List<Task> tasks = new List<Task>();

            Console.WriteLine("Creating container if does not exist");
            var container = client.GetContainerReference(this.container);
            container.CreateIfNotExistsAsync().Wait();

            IReadOnlyList<StorageFile> files = await dir.GetFilesAsync();
            foreach (StorageFile file in files)
            {
                Stream stream = await file.OpenStreamForReadAsync();
                CloudBlockBlob blob = container.GetBlockBlobReference(file.Name);
                blob.DeleteIfExistsAsync().Wait();
                tasks.Add(blob.UploadFromStreamAsync(stream));
            }
            while (tasks.Count > 0)
            {
                tasks.RemoveAll(task => task.IsCompleted);
            }
        }

        // All files in localfolder are first removed to prevent duplicates and 
        // unwanted images from loading into grid, images are then downloaded 
        // into local folder (UWP default)

        public async Task DownloadFiles()
        {
            StorageFolder dest = ApplicationData.Current.LocalFolder;
            var files = await dest.GetFilesAsync();
            foreach (StorageFile file in files)
            {
                await file.DeleteAsync();
            }
            CloudBlobContainer container = client.GetContainerReference(this.container);
            BlobContinuationToken continuationToken = null;
            List<IListBlobItem> results = new List<IListBlobItem>();
            do
            {
                var response = await container.ListBlobsSegmentedAsync(continuationToken);
                continuationToken = response.ContinuationToken;
                results.AddRange(response.Results);
            }
            while (continuationToken != null);
            foreach(CloudBlockBlob item in results)
            {
                StorageFile destFile = await dest.CreateFileAsync(item.Name, CreationCollisionOption.ReplaceExisting);
                await item.DownloadToFileAsync(destFile);  
            }
        }
    }
}
