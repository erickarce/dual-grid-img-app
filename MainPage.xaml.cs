﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using Windows.Storage;
using System.Threading.Tasks;
// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace ImgGripApp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();

            store = new AzureStore("serviceurlgoeshere", 
                "imggripapp");
        }

        private ObservableCollection<Image> _leftGridItems = new ObservableCollection<Image>();

        private ObservableCollection<Image> _rightGridItems = new ObservableCollection<Image>();

        private AzureStore store;

        private int leftPageStart;

        private int leftPageEnd;

        private int rightPageStart;

        private int rightPageEnd;

        private StorageFolder userSelectedDir;

        private IReadOnlyList<StorageFile> pictures;

        public ObservableCollection<Image> LeftGridItems
        {
            get { return this._leftGridItems; }
        }

        public ObservableCollection<Image> RightGridItems
        {
            get { return this._rightGridItems; }
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
        }
        
        private void LoadPagingVars(StorageFolder dir, int pageSize, ref int pageStart, ref int pageEnd) 
        {
            Task t = Task.Run(async () => { pictures = await dir.GetFilesAsync(); });
            t.Wait();
            pageStart = 0;
            pageEnd = 0;
        }

        // Function first uploads files, download files and then loads images into second grid
        private void Button_Click_Upload_Images(object sender, RoutedEventArgs e)
        {
            Task t = Task.Run(async () => {
                await store.UploadFilesAsync(userSelectedDir);
                await store.DownloadFiles();
                await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () => {
                    rightPageStart = 0;
                    rightPageEnd = 0;
                    LoadImagesIntoCollection(RightGridItems, ApplicationData.Current.LocalFolder);
                });
            });  
        }

        private void Button_Click_Select_Folder(object sender, RoutedEventArgs e)
        {   
            LoadImagesIntoCollection(LeftGridItems, null);
        }

        private async void LoadImagesIntoCollection(ObservableCollection<Image> images, StorageFolder folderToLoad)
        {
            StorageFolder selectedFolder = folderToLoad;
            if (folderToLoad == null)
            {
                selectedFolder = await ImageUtil.SelectFolderWithPicker();
                userSelectedDir = selectedFolder;
            }
            if (selectedFolder != null)
            {
                images.Clear();
                LoadPagingVars(selectedFolder, 20, ref leftPageStart, ref leftPageEnd);
                ImageUtil.AddPicsToEnd(pictures, 40, 20, images, ref leftPageStart, ref leftPageEnd);
            }
        }

        private void ScrollViewer_ViewChanged_Left(object sender, ScrollViewerViewChangedEventArgs e)
        {
            ScrollViewer scrollViewer = (ScrollViewer)sender;
            //We have to check if the values are 0.0 because they are both set to this when the scrollviewer loads
            if ((scrollViewer.ScrollableHeight <= scrollViewer.VerticalOffset)
                && (scrollViewer.ScrollableHeight != 0.0 && scrollViewer.VerticalOffset != 0.0))
            {
                ImageUtil.AddPicsToEnd(pictures, 40, 10, LeftGridItems, ref leftPageStart, ref leftPageEnd);
            }
            else if (scrollViewer.VerticalOffset == 0)
            {
                ImageUtil.AddPicsToFront(pictures, 40, 10, LeftGridItems, ref leftPageStart, ref leftPageEnd);
            }
        }

        private void ScrollViewer_ViewChanged_Right(object sender, ScrollViewerViewChangedEventArgs e)
        {
            ScrollViewer scrollViewer = (ScrollViewer)sender;
            //We have to check if the values are 0.0 because they are both set to this when the scrollviewer loads
            if ((scrollViewer.ScrollableHeight <= scrollViewer.VerticalOffset)
                && (scrollViewer.ScrollableHeight != 0.0 && scrollViewer.VerticalOffset != 0.0))
            {
                ImageUtil.AddPicsToEnd(pictures, 60, 20, RightGridItems, ref rightPageStart, ref rightPageEnd);
            }
            else if (scrollViewer.VerticalOffset == 0)
            {
                ImageUtil.AddPicsToFront(pictures, 60, 20, RightGridItems, ref rightPageStart, ref rightPageEnd);
            }
        }
    }

}
